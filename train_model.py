from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC
import argparse
import pickle
import os
APP_ROOT = os.path.dirname(os.path.abspath(__file__))

def train_Models():
    try:
         # print("[INFO] loading face embeddings...")
        data = pickle.loads(open(os.path.join(APP_ROOT,'./output/embeddings.pickle'), "rb").read())
        # print("[INFO] encoding labels...")
        le = LabelEncoder()
        # print("LE:", le)
        labels = le.fit_transform(data["names"])
        # print("labels:", labels)

        # print("[INFO] training model...")
        recognizer = SVC(C=1.0, kernel="linear", probability=True)
        # print("recognizer:", recognizer)
        recognizer.fit(data["embeddings"], labels)
        f = open(os.path.join(APP_ROOT,'./output/recognizer.pickle'), "wb")
        f.write(pickle.dumps(recognizer))
        f.close()
        f = open(os.path.join(APP_ROOT,'./output/le.pickle'), "wb")
        f.write(pickle.dumps(le))
        f.close()
        return True
    except:
        return False
   