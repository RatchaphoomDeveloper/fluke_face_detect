from typing import Text
from imutils.face_utils import FaceAligner
from imutils.face_utils import rect_to_bb
import argparse
import imutils
import dlib
import cv2
import os
from flask import Flask, flash, request, redirect, url_for,jsonify
from werkzeug.utils import secure_filename
from extract_embeddings import embledDatas
from train_model import train_Models
from recognize import recogN
UPLOAD_FOLDER = '/datasets'
ALLOWED_EXTENSIONS = {'pdf', 'png', 'jpg', 'jpeg'}
APP_ROOT = os.path.dirname(os.path.abspath(__file__))

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/register_person', methods = ['POST'])
def upload_files():
	try:
		i = 0
		if request.method == 'POST':
			
			f = request.files["file"]
			name = request.form["folder_name"]
			chk = False
			directory_contents = os.listdir("./datasets")
			for s in directory_contents:
				if s == str(name).lower():
				   chk = True
			if chk == False:
				directory = str(name).lower()
				parent_dir = "./datasets/"
				path = os.path.join(parent_dir, directory)
				os.makedirs(path, exist_ok = True) 
			f.save("./datasets/"+str(name).lower()+"/"+secure_filename("0.png"))
			image = cv2.imread(os.path.join(APP_ROOT,"./datasets/"+str(name).lower()+"/0.png"))
			image = imutils.resize(image, width=400)
			arr = os.listdir("./datasets/"+str(name).lower())
			x = len(arr)
			if x != 0:
				i = (x+1)
			
			for n in range(100):
					cv2.imwrite(os.path.join(APP_ROOT,"./datasets/"+str(name).lower()+"/"+str(i+1)+".png"), image)
					i+=1
			embChk = embledDatas()
			if embChk == True:
				train_Models()
			else:
				return jsonify({"message":"Fail"})
			
		return jsonify({"message":"Register success"})
	except:
		return jsonify({"message":"Register unsuccess"})

@app.route('/recogN_perSoN', methods = ['POST'])
def recogN_perSoN():
	status = []
	if request.method == 'POST':
		f = request.files["file"]
		f.save("./images/"+secure_filename(f.filename))
		status = recogN(f.filename)
	return jsonify(status)




if __name__ == '__main__':
    app.run(host="0.0.0.0",port=5000,debug=True)