import numpy as np
import argparse
import imutils
import pickle
import cv2
import os
APP_ROOT = os.path.dirname(os.path.abspath(__file__))


def recogN(filename):
	print("[INFO] loading face detector...")
	protoPath = os.path.sep.join(['face_detection_model', "deploy.prototxt"])
	modelPath = os.path.sep.join(['face_detection_model',"res10_300x300_ssd_iter_140000.caffemodel"])
	detector = cv2.dnn.readNetFromCaffe(os.path.join(APP_ROOT,"./"+protoPath) ,os.path.join(APP_ROOT,"./"+modelPath) )

	print("[INFO] loading face recognizer...")
	embedder = cv2.dnn.readNetFromTorch(os.path.join(APP_ROOT,'./openface.nn4.small2.v1.t7'))

	recognizer = pickle.loads(open(os.path.join(APP_ROOT,'./output/recognizer.pickle'), "rb").read())
	le = pickle.loads(open(os.path.join(APP_ROOT,'./output/le.pickle'), "rb").read())

	image = cv2.imread(os.path.join(APP_ROOT,'./images/'+filename))
	image = imutils.resize(image, width=600)
	(h, w) = image.shape[:2]

	imageBlob = cv2.dnn.blobFromImage(
		cv2.resize(image, (300, 300)), 1.0, (300, 300),
		(104.0, 177.0, 123.0), swapRB=False, crop=False)

	detector.setInput(imageBlob)
	detections = detector.forward()
	text = ""
	resp = []
	for i in range(0, detections.shape[2]):
		confidence = detections[0, 0, i, 2]

		if confidence > 0.5:
			box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
			(startX, startY, endX, endY) = box.astype("int")

			face = image[startY:endY, startX:endX]
			(fH, fW) = face.shape[:2]

			if fW < 20 or fH < 20:
				continue

			faceBlob = cv2.dnn.blobFromImage(face, 1.0 / 255, (96, 96),
				(0, 0, 0), swapRB=True, crop=False)
			embedder.setInput(faceBlob)
			vec = embedder.forward()

			preds = recognizer.predict_proba(vec)[0]
			j = np.argmax(preds)
			proba = preds[j]
			name = le.classes_[j]

			chk = False
			directory_contents = os.listdir("./datasets")
			for s in directory_contents:
				if s == str(name).lower():
					chk = True
			
			
			if chk == True:
				text = "{}: {:.2f}%".format("unknown" if (proba * 100) < 50 else name,  proba * 100)
			else:
				text = "{}: {:.2f}%".format("unknown",  proba * 100)
			
			resp.append({"name":name,"scores":  text.split(":")[1].strip(" ") , "message":"Please register or add more image for make sure who are you?" if str(name).lower() == "unknown" else "success"})
			
			# y = startY - 10 if startY - 10 > 10 else startY + 10
			# cv2.rectangle(image, (startX, startY), (endX, endY),
			# 	(0, 0, 255), 2)
			# cv2.putText(image, text, (startX, y),
			# 	cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
	return resp

# cv2.imshow("Image", image)
# cv2.waitKey(0)
