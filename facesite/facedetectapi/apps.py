from django.apps import AppConfig


class FacedetectapiConfig(AppConfig):
    name = 'facedetectapi'
